//
//  main.cpp
//  CommandLineTool
//  Software Development for Audio
//

#include <iostream>

#include "Array.hpp"


bool testArray(); //function to test Array class

int main (int argc, const char* argv[])
{
    
    if (testArray() == true)
    {
        std::cout << "all Array tests passed\n";
    }
    else
    {
        std::cout << "Array tests failed\n";
    }
    
    return 0;
}

bool testArray()
{
    Array array;
    const float testArray[] = {0.f, 1.f, 2.f, 3.f, 4.f, 5.f};
    const int testArraySize = sizeof (testArray) / sizeof (testArray[0]);
    
    if (array.size() != 0)
    {
        std::cout << "size is incorrect\n";
        return false;
    }
    
    for (int i = 0; i < testArraySize; i++)
    {
        array.add (testArray[i]);
        
        if (array.size() != i + 1)
        {
            std::cout << "size is incorrect\n";
            return false;
        }
        
        if (array.get (i) != testArray[i])
        {
            std::cout << "value at index "<< i << " recalled incorrectly\n" ;
            return false;
        }
    }
    return true;
}