//
//  Array.hpp
//  CommandLineTool
//
//  Created by Thomas Harvey on 04/10/2016.
//  Copyright © 2016 Tom Mitchell. All rights reserved.
//

#ifndef Array_hpp
#define Array_hpp

#include <stdio.h>

class Array
{
public:
    
    Array();
    ~Array();
    void add (float itemValue);
    float get (int index);
    int size();
    
private:
    
    float *floatPoint;
    int numItems;
    
};


#endif /* Array_hpp */
