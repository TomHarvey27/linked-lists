//
//  Array.cpp
//  CommandLineTool
//
//  Created by Thomas Harvey on 04/10/2016.
//  Copyright © 2016 Tom Mitchell. All rights reserved.
//

#include "Array.hpp"
#include <iostream>

Array::Array()
{
    floatPoint = nullptr;
    numItems = 0;
    
}

Array::~Array()
{
    delete [] floatPoint;
    
}
void Array::add(float itemValue) // A new value is passed in
{
    numItems ++; // for example that value could be 4th element in an array, but there are currently only 3
    
    float* temp = nullptr;      // a tempoary array is created with extra space for the new value
    temp = new float[numItems];
    
    for (int count= 0 ; count < numItems -1 ; count++) //each value from the current array is coppied into the temporary
    {
        temp[count] = floatPoint[count]; //The temp now has all the same as the previous
    }
    
    temp[numItems-1] = itemValue; // now it adds on the last one to the temp list
    floatPoint = temp; //it then copies it back to the floating point arrayx
    
    
    
    
    
}

float Array::get (int index)
{
    float item = 0;
    
    if (index == 0)
    {
    
        return 0;
    }
    
    item = floatPoint[index];
    
    return item;
}

int Array::size()
{
    int numOfArrays = numItems;
    
    return numOfArrays;
}
