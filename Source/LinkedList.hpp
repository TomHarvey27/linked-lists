//
//  LinkedList.hpp
//  CommandLineTool
//
//  Created by Thomas Harvey on 06/10/2016.
//  Copyright © 2016 Tom Mitchell. All rights reserved.
//

#ifndef LinkedList_hpp
#define LinkedList_hpp

#include <stdio.h>



class LinkedList
{
public:
    
    LinkedList();
    ~LinkedList();
    void add (float itemValue);
    float get (int index);
    int size();
    
    
    
private:
    
    struct Node
    {
        float value; // the data of the node
        Node* next; // a reference which will point to the next node.
        
    };
    
    Node* head;
    Node value;
    
    
    
    int numOfItems;
    
    
    
    
    
};

#endif /* LinkedList_hpp */

